# Tasks Frontend

This project aims to explore images of Mars. We have provided you with the latest Mars images, allowing you to have an exciting experience of viewing the Mars landscape. Furthermore, we have enabled camera filtering options to enhance your experience.

## Project Setup

```sh
npm run install
```

### Compile and Hot-Reload for Development

```sh
npm run serve
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
