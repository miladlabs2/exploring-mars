const { VUE_APP_BASE_URL, VUE_APP_API_TOKEN } = process.env;

const host = VUE_APP_BASE_URL;

const API_KEY_PARAM = `api_key=${VUE_APP_API_TOKEN}`;
const MARS_PHOTOS_API_PATH = "mars-photos/api/v1/rovers/curiosity/photos";
const SOL_PARAM = "sol=1000";
const TOTAL_PHOTOS_PARAM = "total_photos=1000";
const PAGE_PARAM = "page=";

export default {
  async getMarsPhotos(page) {
    if (!page) page = 1;
    const url = `${host}${MARS_PHOTOS_API_PATH}?${SOL_PARAM}&${TOTAL_PHOTOS_PARAM}&${PAGE_PARAM}${page}&${API_KEY_PARAM}`;
    const response = await fetch(url);
    return response.json();
  },
};
