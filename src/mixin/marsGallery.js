import API from "../api";
//import mockdata from "./mock.json";

export default {
  data() {
    return {
      loading: true,
      pageNumber: 1,
      photos: [],
      filters: {
        camera: ["FHAZ", "RHAZ", "MAST", "CHEMCAM", "MAHLI", "MARDI", "RHAZ"],
      },
    };
  },
  computed: {
    categories() {
      return this.countCategories(this.photos);
    },
    marsPhotos() {
      // set filters are added on the objects of Mars photos
      return this.photos.filter((row) =>
        this.filters.camera.includes(row.camera.name)
      );
    },
  },
  methods: {
    toggleCategory(name) {
      const { camera } = this.filters;
      const filters = camera.includes(name)
        ? camera.filter((item) => item !== name)
        : [...camera, name];
      this.filters.camera = filters;
      this.pageNumber++;
    },
    countCategories(arr) {
      const nameCountMap = new Map();
      // Iterate over each object in the input array and add the camera name to
      arr.forEach((obj) => {
        const cameraName = obj.camera.name;
        if (nameCountMap.has(cameraName)) {
          nameCountMap.set(cameraName, nameCountMap.get(cameraName) + 1);
        } else {
          nameCountMap.set(cameraName, 1);
        }
      });
      // Display the final result
      return Object.fromEntries(nameCountMap);
    },
    async loadMorePhotos() {
      this.pageNumber++;
      await this.fetchMarsPhotos();
    },
    async fetchMarsPhotos() {
      this.loading = true;
      const { photos } = await API.getMarsPhotos(this.pageNumber);
      //const photos = mockdata;
      this.photos.push(...photos);
      this.loading = false;
    },
  },
  async created() {
    await this.fetchMarsPhotos();
  },
};
